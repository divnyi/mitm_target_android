package com.example.mitm_target_android

import android.os.Bundle
import android.os.StrictMode
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request
import javax.net.ssl.HostnameVerifier
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            runRequests()
        }
    }

    var urls = arrayOf(
        "http://httpbin.org/user-agent",
        "https://httpbin.org/user-agent",
        "https://postman-echo.com/get?foo1=bar1&foo2=bar2"
    )

    fun runRequests() {
        GlobalScope.launch(Dispatchers.Default) {
            GlobalScope.launch(Dispatchers.Main) {
                label.text = "loading..."
            }

            var responses = ""
            for (url in urls) {
                responses += performGetRequest(url)
            }

            GlobalScope.launch(Dispatchers.Main) {
                label.text = responses
            }
        }
    }

    suspend fun performGetRequest(url: String): String {
        return suspendCoroutine {
            try {
                val request = Request.Builder()
                    .url(url)
                    .build()

                val builder = OkHttpClient.Builder()
                builder.hostnameVerifier(HostnameVerifier { hostname, session -> true })

                val client = builder.build()

                client.newCall(request).execute().use { response ->
                    var message = "url: ${response.request.url}\n" +
                            "code: ${response.code}\n" +
                            "body: ${response.body?.string()}"
                    it.resume(message + "\n\n")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                it.resume((e.localizedMessage ?: "") + "\n\n")
            }
        }
    }
}
